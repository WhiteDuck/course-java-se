package by.epam.ex_11;

import by.epam.ex_11.animals.Animal;
import by.epam.ex_11.animals.Cat;
import by.epam.ex_11.animals.Dog;

public class Runner {
    public static void main(String args[]) {
        Animal cat = new Cat("Vasya");
        Animal dog = new Dog("Barsick");

        System.out.println(cat.say());
        System.out.println(dog.say());
    }
}
