package by.epam.ex_13.books;

public class Book {
    private String name;
    private int year;

    public Book() { }

    public Book(String name, int year) {
        this.setName(name);
        this.setYear(year);
    }

    public void setName(String name){
        this.name = name;
    }

    public void setYear(int year) {
        if(year > 581) {
            this.year = year;
        } else {
            System.out.println("Invalid Year");
            this.year = 0;
        }

    }

    public String getName() {
        return this.name;
    }

    public int getYear() {
        return this.year;
    }

    @Override
    public String toString() {
        return "name: " + this.getName() + " year: " + this.getYear();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        } else if(obj == null) {
            return false;
        }
        Book eBook = (Book)obj;
        if(this.getYear() == eBook.getYear() || this.getName() == eBook.getName()) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return !this.name.isEmpty() ? this.name.hashCode() : 0 + this.getYear() * 151;
    }

    public Book(Book book) {
        this(book.name, book.year);
    }
}
