package by.epam.ex_5.books;

public class InterestingBook extends Book {
    private int interest;

    public int getInterest() {
        return this.interest;
    }

    public void setInterest(int interest) {
        this.interest = interest;
    }

    //плохой пример, но для наглядности...
    public void setInterest(int... interests) {
        int overall = 0;
        for(int value : interests) {
            overall += value;
        }
        this.setInterest(overall / interests.length);
    }

    @Override
    public String getName() {
        return "Interesting " + super.getName();
    }

    public InterestingBook(String name, int year, int interest) {
        super(name, year);
        this.setInterest(interest);
    }
}
