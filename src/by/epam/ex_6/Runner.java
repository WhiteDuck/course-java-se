package by.epam.ex_6;

import by.epam.ex_6.books.Book;
import by.epam.ex_6.books.InterestingBook;

public class Runner {
    public static void main(String args[]) {
        Book book = new Book("JAVA", 1998);
        InterestingBook interestingBook = new InterestingBook("JAVA Second Ed", 2016, 100);

        System.out.println(book.getName());
        System.out.println(interestingBook.getName());
    }
}
