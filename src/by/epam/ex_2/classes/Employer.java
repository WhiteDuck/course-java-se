package by.epam.ex_2.classes;

public class Employer {
    private String name;
    private int age;

    public Employer() {
        this.name = "Vasya";
        this.age = 25;
    }

    public Employer(String name, int age) {
        this.setName(name);
        this.setAge(age);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        if(age > 0) {
            this.age = age;
        } else {
            System.out.println("Invalid Age");
            this.age = 0;
        }
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }
}
