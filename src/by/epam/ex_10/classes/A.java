package by.epam.ex_10.classes;

public class A {
    static {
        System.out.println("A: Static");
    }

    {
        System.out.println("A: Initialization");
    }

    public A() {
        System.out.println("A: Constructor");
    }
}
