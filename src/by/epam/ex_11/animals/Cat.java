package by.epam.ex_11.animals;

public class Cat extends Animal {
    @Override
    public String say() {
        return "Meow";
    }

    public Cat(String name) {
        super(name);
    }
}
