package by.epam.ex_13;

import by.epam.ex_13.books.Book;

public class Runner {
    public static void main(String args[]) {
        Book book = new Book("Java", 2010);
        Book book1 = new Book("Java", 2010);
        System.out.println(book); //toString
        System.out.println(book == book1);
        System.out.println(book.equals(book1));
        System.out.println(book.hashCode());

        Book book2 = new Book(book);
        System.out.println(book2); //toString
    }
}
