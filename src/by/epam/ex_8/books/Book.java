package by.epam.ex_8.books;

final public class Book {
    final private String name;
    final private int year;

    {
       this.name = "C#";
       this.year = 2015;
    }

    public Book() {

    }

    public String getName() {
        return this.name;
    }

    public int getYear() {
        return this.year;
    }
}
