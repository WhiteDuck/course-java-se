package by.epam.ex_3;


import by.epam.ex_3.books.Book;
import by.epam.ex_3.books.InterestingBook;

public class Runner {
    public static void main(String args[]) {
        Book book = new Book("Book", 1998);
        InterestingBook interestingBook = new InterestingBook("Interesting Book", 2016, 100);

        System.out.println(book.getName());
        System.out.println(interestingBook.getName());
    }
}
