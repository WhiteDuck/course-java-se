package by.epam.ex_14.enums;

public enum Animals {
    CAT{
        public int getCost() {
            return 1150;
        }
    },
    DOG {
        public int getCost() {
            return 1000;
        }
    },
    DUCK {
        public int getCost() {
            return 2000;
        }
    },
    COW {
        public int getCost() {
            return 50000;
        }
    };

    abstract public int getCost();
}
