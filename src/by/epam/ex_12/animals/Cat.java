package by.epam.ex_12.animals;

import by.epam.ex_12.animals.interfaces.IPurrable;

public class Cat extends Animal implements IPurrable{
    @Override
    public String say() {
        return "Meow";
    }

    public Cat(String name) {
        super(name);
    }

    @Override
    public String purr() {
        return "Myrlick";
    }
}
