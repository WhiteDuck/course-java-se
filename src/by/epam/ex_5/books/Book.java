package by.epam.ex_5.books;

public class Book {
    /*final*/ private String name = "C++";
    private int year;

    public Book() { }

    public Book(String name, int year) {
        this.setName(name);
        this.setYear(year);
    }

    public void setName(String name){
        this.name = name; // Тут ошибка
    }

    public void setYear(int year) {
        if(year > 581) {
            this.year = year;
        } else {
            System.out.println("Invalid Year");
            this.year = 0;
        }

    }

    public String getName() {
        return this.name;
    }

    public int getYear() {
        return this.year;
    }
}
