package by.epam.ex_10;

import by.epam.ex_10.classes.A;
import by.epam.ex_10.classes.B;

public class Runner {
    public static void main(String args[]) {
        System.out.println("A:");
        A a = new A();
        System.out.println("B:");
        B b = new B();
    }
}
