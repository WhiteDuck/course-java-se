package by.epam.ex_9.math;

public class Math {
    public static int sum(int... args) {
        int sum = 0;
        for(int value : args){
            sum += value;
        }
        return sum;
    }
}
