package by.epam.ex_3.books;

public class InterestingBook extends Book {
    private int interest;

    public int getInterest() {
        return this.interest;
    }

    public void setInterest(int interest) {
        this.interest = interest;
    }

    public InterestingBook(String name, int year, int interest) {
        super(name, year);
        this.setInterest(interest);
    }
}
