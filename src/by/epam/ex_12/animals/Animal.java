package by.epam.ex_12.animals;

abstract public class Animal {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Animal(String name) {
        this.setName(name);
    }

    abstract public String say();
}
