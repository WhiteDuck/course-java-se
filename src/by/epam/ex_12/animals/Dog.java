package by.epam.ex_12.animals;

public class Dog extends Animal {
    @Override
    public String say() {
        return "Woof";
    }

    public Dog(String name) {
        super(name);
    }
}
