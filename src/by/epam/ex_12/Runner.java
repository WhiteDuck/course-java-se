package by.epam.ex_12;

import by.epam.ex_12.animals.Animal;
import by.epam.ex_12.animals.Cat;
import by.epam.ex_12.animals.Dog;
import by.epam.ex_12.animals.interfaces.IPurrable;

public class Runner {
    public static void main(String args[]) {
        IPurrable cat = new Cat("Vasya");
        Animal dog = new Dog("Barsick");

        System.out.println(cat.purr());
        System.out.println(dog.say());
    }
}
