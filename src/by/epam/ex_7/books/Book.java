package by.epam.ex_7.books;

/*final*/ public class Book {
    private String name;
    private int year;

    public Book() { }

    public Book(String name, int year) {
        this.setName(name);
        this.setYear(year);
    }

    public void setName(String name){
        this.name = name;
    }

    public void setYear(int year) {
        if(year > 581) {
            this.year = year;
        } else {
            System.out.println("Invalid Year");
            this.year = 0;
        }

    }

    public String getName() {
        return this.name;
    }

    public int getYear() {
        return this.year;
    }
}
