package by.epam.ex_10.classes;

public class B extends A {
    static {
        System.out.println("B: Static");
    }

    {
        System.out.println("B: Initialization");
    }

    public B() {
        System.out.println("B: Constructor");
    }
}
