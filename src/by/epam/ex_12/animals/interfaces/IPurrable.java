package by.epam.ex_12.animals.interfaces;

public interface IPurrable {
    String purr();
}
